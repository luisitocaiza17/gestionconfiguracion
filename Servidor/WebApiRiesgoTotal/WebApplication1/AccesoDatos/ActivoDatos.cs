﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;

namespace WebApplication1.AccesoDatos
{
    public class ActivoDatos
    {
        public static List<ACTIVO> ObtenerActivos()
        {
            using (RIESGO_TOTAL_Entities model = new RIESGO_TOTAL_Entities())
            {
                List<ACTIVO> entidad = model.ACTIVO.ToList();
                return entidad;
            }
        }
        public static ACTIVO ObtenerActivo(int id)
        {
            using (RIESGO_TOTAL_Entities model = new RIESGO_TOTAL_Entities())
            {
                ACTIVO entidad = model.ACTIVO.Where(x => x.ID_ACTIVO == id).FirstOrDefault();
                return entidad;
            }
        }

    }
}