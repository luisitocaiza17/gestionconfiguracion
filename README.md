# Riesgo Total App
Es una aplicación que permite calcular el riesgo areas de tecnologías, asi como su análisis estadistico en base a los resultados obtenidos.

## Componentes
- #### Interfaz:
	Aplicación donde se encuentran todos los elementos visuales de la aplicación, comunicación con servicios y procesos de validación, esta construido en **typescript,  html, css**.
- #### Servidor:
	Aplicación donde se encuentran las funciones internaz del servidor, conección a la base de datos, logica y servicios, está construido en **c#, entity Framework, servicios rest**.
- #### Base Datos:
	Script de base de datos con la estructura de base(Tablas) hecho en TSQL.

## Instalación
Descargue el codigo fuente desde el servidor de GitLab : [RiesgoTotal](https://gitlab.com/luisitocaiza17/gestionconfiguracion.git "RiesgoTotal")
En un lugar destro de su computador, es recomendable usar git para realizar el proceso, para poder levantar el proyecto tambien necesita tener instalada las siguientes aplicaciones:
- [Visual Studio 2017](https://visualstudio.microsoft.com/es/downloads/ "Visual Studio 2017")
- [SQL Server 2017](https://www.microsoft.com/en-us/sql-server/sql-server-2017 "SQL Server 2017")
- [Fiddler](https://www.telerik.com/fiddler "Fiddler")

Dentro de la carpeta de descarga encontrará tres carpetas:
1. Interfaz
2. Servicios
3. Base de Datos

#### Interfaz: 
En la carpeta Interfaz encontrá el proyecto: AppRiesgoTotal que es la aplicación que contiene el codigo fuente visual de la aplicación, dentro de la carpeta encontrará el archivo *RiegoTotal.sln* la que debe ser abierta con Visual Studio.
#### Servidor:
En la carpeta Servidor encontrará el proyecto: WebApiRiesgoTotal que es la aplicación que contiene el código fuente del servidor de la aplicació, donde se encuentrar los elementos como conección a la base de datos, procesos de logica, y levantamiento de servicios, dentro de la carpeta encontrará el archivo *WebApiRiesgoTotal.sln* la que debe ser abierta con Visual Studio.
#### Base de Datos
En la carpeta Base de Datos encontrará los scripts que generará la base de datos **Riesgo_Total** con sus respectivas tablas y sus registros.

## Uso de la Aplicación
- [![Inicio](https://ibb.co/vk5kXMf "Inicio")](https://ibb.co/vk5kXMf "Inicio")
- [![Activos](https://ibb.co/5BfTRCL "Activos")](https://ibb.co/5BfTRCL "Activos")
- [![Análisis](https://ibb.co/Smv8R8n "Análisis")](https://ibb.co/Smv8R8n "Análisis")